<?php

namespace Drupal\apstyle_augment\Plugin\DateAugmenter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\date_augmenter\DateAugmenter\DateAugmenterPluginBase;
use Drupal\date_augmenter\Plugin\PluginFormTrait;

/**
 * Date Augmenter plugin adjust date formats meet to AP Stylebook guidelines.
 *
 * @DateAugmenter(
 *   id = "apstyle",
 *   label = @Translation("Associated Press Stylebook"),
 *   description = @Translation("Adjust output to meet the Associated Press Stylebook guidelines"),
 *   weight = 0
 * )
 */
class APStyle extends DateAugmenterPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  protected $config;
  protected $output;

  /**
   * Builds and returns a render array for the task.
   *
   * @param array $output
   *   The existing render array, to be augmented, passed by reference.
   * @param Drupal\Core\Datetime\DrupalDateTime $start
   *   The object which contains the start time.
   * @param Drupal\Core\Datetime\DrupalDateTime $end
   *   The optionalobject which contains the end time.
   * @param array $options
   *   An array of options to further guide output.
   */
  public function augmentOutput(array &$output, DrupalDateTime $start, DrupalDateTime $end = NULL, array $options = []) {
    $config = $options['settings'] ?? $this->getConfiguration();

    // Convert meridians to be lowercase and include periods.
    if ($config['meridian']) {
      // Check for php meridian.
      $start_time = $output['start']['#text']['time'];
      if (preg_match('(a|A)', $start_time['#format']['#markup']) === 1) {
        $output['start']['#text']['time']['value']['#markup'] = str_ireplace(['am', 'pm'], ['a.m.', 'p.m.'], $start_time['value']['#markup']);
      }
      $end_time = $output['end']['#text']['time'];
      if (preg_match('(a|A)', $end_time['#format']['#markup']) === 1) {
        $output['end']['#text']['time']['value']['#markup'] = str_ireplace(['am', 'pm'], ['a.m.', 'p.m.'], $end_time['value']['#markup']);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'meridian' => TRUE,
    ];
  }

  /**
   * Create configuration fields for the plugin form, or injected directly.
   *
   * @param array $form
   *   The form array.
   * @param array $settings
   *   The setting to use as defaults.
   *
   * @return array
   *   The updated form array.
   */
  public function configurationFields(array $form, ?array $settings) {
    if (empty($settings)) {
      $settings = $this->defaultConfiguration();
    }
    $form['meridian'] = [
      '#title' => $this->t('Convert <em>ante meridiem</em> and <em>post meridiem</em>.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['meridian'],
      '#description' => $this->t('Meridians should use periods and lower case letters. Converts `a` and `A` time formats'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->configurationFields($form, $this->configuration);

    return $form;
  }

}
