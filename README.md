CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers


INTRODUCTION
------------

This module provides a Date Augmenter plugin, that adjusts date output to meet the Associated Press Stylebook guidelines.


INSTALLATION
------------

 * Install this module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.


REQUIREMENTS
------------

This module requires the Date Augmenter module, and a compatible formatter.


MAINTAINERS
-----------

 * Current Maintainer: Joe Whitsitt (joewhitsitt) - https://www.drupal.org/u/joewhitsitt
